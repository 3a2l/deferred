#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <assert.h>

constexpr size_t MAX_PATH_LENGTH = 1024;

size_t
read_file(const char* filepath, unsigned char** buffer)
{
	assert(filepath);

	int file_descriptor = open(filepath, O_RDONLY);

	struct stat st;
	fstat(file_descriptor, &st);
	off_t buffer_size = st.st_size;

	*buffer = new unsigned char[buffer_size];
	read(file_descriptor, *buffer, buffer_size);

	close(file_descriptor);

	return buffer_size;
}
