#version 450

const vec3 vertices[] =
{
    vec3( 0.0f,  0.5f, 0.0f),
    vec3( 0.5f, -0.5f, 0.0f),
    vec3(-0.5f, -0.5f, 0.0f),
};

layout(location=0)out vec4 vertexColour;

void main()
{
    gl_Position = vec4(vertices[gl_VertexIndex], 1.0f);
    vertexColour = gl_Position + 0.5f;
}
