#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <assert.h>

constexpr size_t MAX_PATH_LENGTH = 1024;

//static void
//to_utf8(char* output, const wchar_t* input)
//{
//    int buffer_size = WideCharToMultiByte(CP_UTF8, 0, input, -1, output, MAX_PATH_LENGTH, nullptr, FALSE);
//    assert(buffer_size <= MAX_PATH_LENGTH);
//}

static void
to_utf16(wchar_t* output, const char* input)
{
    [[maybe_unused]] int buffer_size = MultiByteToWideChar(CP_UTF8, 0, input, -1, output, MAX_PATH_LENGTH);
    assert(buffer_size <= MAX_PATH_LENGTH);
}

size_t
read_file(const char* filepath, unsigned char** buffer)
{
    assert(filepath);

    wchar_t filepath_win[MAX_PATH_LENGTH] = {};
    to_utf16(filepath_win, filepath);

    HANDLE file_handle = CreateFile(filepath_win, GENERIC_READ,
                                    0, nullptr,
                                    OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
    assert(file_handle != INVALID_HANDLE_VALUE);

    DWORD buffer_size = GetFileSize(file_handle, nullptr);
    *buffer = new unsigned char[buffer_size];

    DWORD bytes_read = 0;
    if (ReadFile(file_handle, *buffer, buffer_size, &bytes_read, nullptr) == FALSE)
    {
        CloseHandle(file_handle);
        return 0;
    }

    CloseHandle(file_handle);
    return bytes_read;
}

size_t
write_file(const char* filepath, const unsigned char* buffer, const size_t buffer_size)
{
    assert(filepath && buffer && buffer_size);

    wchar_t filepath_win[MAX_PATH_LENGTH] = {};
    to_utf16(filepath_win, filepath);

    HANDLE file_handle = CreateFile(filepath_win, GENERIC_WRITE,
                                    0, nullptr,
                                    CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
    assert(file_handle != INVALID_HANDLE_VALUE);

    DWORD bytes_written = 0;
    if (WriteFile(file_handle, buffer, (DWORD)buffer_size, &bytes_written, NULL) == FALSE)
    {
        CloseHandle(file_handle);
        return 0;
    }

    CloseHandle(file_handle);
    return bytes_written;
}
