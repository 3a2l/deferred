#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <vulkan/vulkan.h>

extern size_t read_file(const char* filepath, unsigned char** buffer);
extern size_t write_file(const char* filepath, const unsigned char* buffer, const size_t buffer_size);

template<typename T, size_t N>
consteval size_t countof(const T(&)[N]) noexcept
{
	return N;
}

static uint32_t QueueFamilyIndex = 0;
static VkPhysicalDevice PhysicalDevice = VK_NULL_HANDLE;

static VkInstance
create_instance()
{
	constexpr VkApplicationInfo application_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = "DeferredApplication",
		.applicationVersion = 1,
		.pEngineName = "DeferredEngine",
		.engineVersion = 1,
		.apiVersion = VK_API_VERSION_1_1,
	};

#ifndef NDEBUG
	constexpr const char* validation_layers[] = {
		"VK_LAYER_KHRONOS_validation",
	};
#endif

	constexpr const char* extensions[] = {
		VK_KHR_SURFACE_EXTENSION_NAME,
#ifdef VK_USE_PLATFORM_WIN32_KHR
		VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#endif
#ifdef VK_USE_PLATFORM_XLIB_KHR
		VK_KHR_XLIB_SURFACE_EXTENSION_NAME,
#endif
	};

	const VkInstanceCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &application_info,
#ifndef NDEBUG
		.enabledLayerCount = countof(validation_layers),
		.ppEnabledLayerNames = validation_layers,
#endif
		.enabledExtensionCount = countof(extensions),
		.ppEnabledExtensionNames = extensions,
	};

	VkInstance instance = VK_NULL_HANDLE;
	const VkResult create = vkCreateInstance(&create_info, nullptr, &instance);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan instance.\n");
	}

	return instance;
}

static VkDevice
create_device(const VkInstance instance)
{
	assert(instance);

	VkPhysicalDevice physical_devices[16] = {};
	uint32_t physical_device_count = countof(physical_devices);
	vkEnumeratePhysicalDevices(instance, &physical_device_count, physical_devices);
	assert(physical_device_count > 0 && physical_device_count <= 16);

	for (uint32_t i = 0; i < physical_device_count; ++i)
	{
		VkPhysicalDeviceProperties properties = {};
		vkGetPhysicalDeviceProperties(physical_devices[i], &properties);
		if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		{
			PhysicalDevice = physical_devices[i];
			break;
		}
	}
	if (PhysicalDevice == VK_NULL_HANDLE)
	{
		fprintf(stdout, "Using the first GPU as a fallback.\n");
		PhysicalDevice = physical_devices[0];
	}

	VkQueueFamilyProperties queue_families[16] = {};
	uint32_t queue_families_count = countof(queue_families);
	vkGetPhysicalDeviceQueueFamilyProperties(PhysicalDevice, &queue_families_count, queue_families);
	assert(queue_families_count > 0 && queue_families_count <= 16);

	for (uint32_t i = 0; i < queue_families_count; ++i)
	{
		if ((queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0)
		{
			assert(queue_families[i].queueCount > 0);
			QueueFamilyIndex = i;
			break;
		}
	}

	constexpr float queue_priorities[] = { 1.0f };
	const VkDeviceQueueCreateInfo queue_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.queueFamilyIndex = QueueFamilyIndex,
		.queueCount = 1,
		.pQueuePriorities = queue_priorities,
	};

	constexpr const char* extensions[] = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	};

	const VkDeviceCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &queue_create_info,
		.enabledExtensionCount = countof(extensions),
		.ppEnabledExtensionNames = extensions,
	};

	VkDevice device = VK_NULL_HANDLE;
	const VkResult create = vkCreateDevice(PhysicalDevice, &create_info, nullptr, &device);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan device.\n");
	}

	return device;
}

static VkSurfaceKHR
create_surface(const VkInstance instance, GLFWwindow* window)
{
	assert(instance && window);

	VkSurfaceKHR surface = VK_NULL_HANDLE;

#ifdef VK_USE_PLATFORM_WIN32_KHR
	const VkWin32SurfaceCreateInfoKHR create_info = {
		.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
		.hinstance = GetModuleHandle(nullptr),
		.hwnd = glfwGetWin32Window(window),
	};

	const VkResult create = vkCreateWin32SurfaceKHR(instance, &create_info, nullptr, &surface);
#endif

#ifdef VK_USE_PLATFORM_XLIB_KHR
	const VkXlibSurfaceCreateInfoKHR create_info = {
		.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
		.dpy = glfwGetX11Display(),
		.window = glfwGetX11Window(window),
	};

	const VkResult create = vkCreateXlibSurfaceKHR(instance, &create_info, nullptr, &surface);
#endif

	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan surface.\n");
	}

	return surface;
}

static VkSwapchainKHR
create_swapchain(const VkDevice device,
				 const VkSurfaceKHR surface, const VkSurfaceCapabilitiesKHR surface_capabilities,
				 const VkSwapchainKHR old_swapchain)
{
	assert(device && surface);

	VkSurfaceFormatKHR formats[16] = {};
	uint32_t formats_count = countof(formats);
	vkGetPhysicalDeviceSurfaceFormatsKHR(PhysicalDevice, surface, &formats_count, formats);

	VkSurfaceFormatKHR format = {};
	for (uint32_t i = 0; i < formats_count; ++i)
	{
		if (formats[i].format == VK_FORMAT_B8G8R8A8_UNORM)
		{
			format = formats[i];
			break;
		}
	}

	VkSwapchainKHR swapchain = VK_NULL_HANDLE;

	const VkSwapchainCreateInfoKHR create_info = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = surface,
		.minImageCount = surface_capabilities.minImageCount + 1,
		.imageFormat = format.format,
		.imageColorSpace = format.colorSpace,
		.imageExtent = surface_capabilities.currentExtent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 1,
		.pQueueFamilyIndices = &QueueFamilyIndex,
		.preTransform = surface_capabilities.currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = VK_PRESENT_MODE_FIFO_KHR,
		.clipped = VK_TRUE,
		.oldSwapchain = old_swapchain,
	};

	const VkResult create = vkCreateSwapchainKHR(device, &create_info, nullptr, &swapchain);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan swapchain.\n");
	}

	return swapchain;
}

static VkSemaphore
create_semaphore(const VkDevice device)
{
	assert(device);

	VkSemaphore semaphore = VK_NULL_HANDLE;

	constexpr VkSemaphoreCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
	};

	const VkResult create = vkCreateSemaphore(device, &create_info, nullptr, &semaphore);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan semaphore.\n");
	}

	return semaphore;
}

static VkCommandPool
create_command_pool(const VkDevice device)
{
	assert(device);

	VkCommandPool command_pool = VK_NULL_HANDLE;

	const VkCommandPoolCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
		.queueFamilyIndex = QueueFamilyIndex,
	};

	const VkResult create = vkCreateCommandPool(device, &create_info, nullptr, &command_pool);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan command pool.\n");
	}

	return command_pool;
}

static VkCommandBuffer
create_command_buffer(const VkDevice device, const VkCommandPool pool)
{
	assert(device && pool);

	VkCommandBuffer command_buffer = VK_NULL_HANDLE;

	const VkCommandBufferAllocateInfo allocate_info = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.commandPool = pool,
			.commandBufferCount = 1,
	};
	const VkResult allocate = vkAllocateCommandBuffers(device, &allocate_info, &command_buffer);
	if (allocate != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to allocate a Vulkan command buffer.\n");
	}

	return command_buffer;
}

static VkRenderPass
create_render_pass(const VkDevice device)
{
	assert(device);

	VkRenderPass render_pass = VK_NULL_HANDLE;

	constexpr VkAttachmentDescription attachments[1] = {
		{
			.format = VK_FORMAT_B8G8R8A8_UNORM,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
		}
	};

	constexpr VkAttachmentReference color_attachments[1] = {
		{
			.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
		}
	};

	const VkSubpassDescription subpasses[1] = {
		{
			.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
			.colorAttachmentCount = countof(color_attachments),
			.pColorAttachments = color_attachments,
		}
	};

	const VkRenderPassCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.attachmentCount = countof(attachments),
		.pAttachments = attachments,
		.subpassCount = countof(subpasses),
		.pSubpasses = subpasses,
	};

	const VkResult create = vkCreateRenderPass(device, &create_info, nullptr, &render_pass);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan render pass.\n");
	}

	return render_pass;
}

static VkImageView
create_image_view(const VkDevice device, const VkImage image)
{
	assert(device && image);

	VkImageView image_view = VK_NULL_HANDLE;

	const VkImageViewCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.image = image,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.format = VK_FORMAT_B8G8R8A8_UNORM,
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.levelCount = 1,
			.layerCount = 1,
		},
	};

	const VkResult create = vkCreateImageView(device, &create_info, nullptr, &image_view);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan image view.\n");
	}

	return image_view;
}

static VkFramebuffer
create_framebuffer(const VkDevice device, const VkRenderPass render_pass,
				   const VkImageView image_view,
				   const uint32_t width, const uint32_t height)
{
	assert(device);

	VkFramebuffer framebuffer = VK_NULL_HANDLE;

	const VkFramebufferCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
		.renderPass = render_pass,
		.attachmentCount = 1,
		.pAttachments = &image_view,
		.width = width,
		.height = height,
		.layers = 1,
	};

	const VkResult create = vkCreateFramebuffer(device, &create_info, nullptr, &framebuffer);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan framebuffer.\n");
	}

	return framebuffer;
}

static VkShaderModule
create_shader(const VkDevice device, const char* shader_filepath)
{
	VkShaderModule shader_module = VK_NULL_HANDLE;

	unsigned char* buffer = nullptr;
	const size_t buffer_size = read_file(shader_filepath, &buffer);
	assert(buffer && buffer_size > 0);

	const VkShaderModuleCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = buffer_size,
		.pCode = reinterpret_cast<uint32_t*>(buffer),
	};

	const VkResult create = vkCreateShaderModule(device, &create_info, nullptr, &shader_module);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan shader module.\n");
	}

	delete[] buffer;
	return shader_module;
}

static VkPipelineLayout
create_pipeline_layout(const VkDevice device)
{
	assert(device);

	VkPipelineLayout pipeline_layout = VK_NULL_HANDLE;

	constexpr VkPipelineLayoutCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
	};

	const VkResult create = vkCreatePipelineLayout(device, &create_info, nullptr, &pipeline_layout);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan pipeline layout.\n");
	}

	return pipeline_layout;
}

static VkPipeline
create_pipeline(const VkDevice device,
				const VkPipelineCache cache,
				const VkPipelineLayout layout,
				const VkRenderPass render_pass,
				const VkShaderModule vertex_module,
				const VkShaderModule fragment_module)
{
	assert(device &&
			// cache &&
			vertex_module &&
			fragment_module && layout && render_pass);

	VkPipeline pipeline = VK_NULL_HANDLE;

	const VkPipelineShaderStageCreateInfo stages[2] = {
		{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.stage = VK_SHADER_STAGE_VERTEX_BIT,
			.module = vertex_module,
			.pName = "main",
		},
		{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
			.module = fragment_module,
			.pName = "main",
		},
	};

	constexpr VkPipelineVertexInputStateCreateInfo vertex_input_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
	};

	constexpr VkPipelineInputAssemblyStateCreateInfo input_assembly_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
	};

	constexpr VkPipelineViewportStateCreateInfo viewport_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.viewportCount = 1,
		.scissorCount = 1,
	};

	constexpr VkPipelineRasterizationStateCreateInfo rasterization_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		.lineWidth = 1.0f,
	};

	constexpr VkPipelineMultisampleStateCreateInfo multisample_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
	};

	constexpr VkPipelineDepthStencilStateCreateInfo depth_stencil_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
	};

	constexpr VkPipelineColorBlendAttachmentState color_blend_attachments[1] = {
		{
			.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
							  VK_COLOR_COMPONENT_G_BIT |
							  VK_COLOR_COMPONENT_B_BIT |
							  VK_COLOR_COMPONENT_A_BIT,
		},
	};

	const VkPipelineColorBlendStateCreateInfo color_blend_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.attachmentCount = countof(color_blend_attachments),
		.pAttachments = color_blend_attachments,
	};

	constexpr VkDynamicState dynamic_states[2] = {
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_SCISSOR,
	};

	const VkPipelineDynamicStateCreateInfo dynamic_state = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.dynamicStateCount = countof(dynamic_states),
		.pDynamicStates = dynamic_states,
	};

	const VkGraphicsPipelineCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.stageCount = countof(stages),
		.pStages = stages,
		.pVertexInputState = &vertex_input_state,
		.pInputAssemblyState = &input_assembly_state,
		.pViewportState = &viewport_state,
		.pRasterizationState = &rasterization_state,
		.pMultisampleState = &multisample_state,
		.pDepthStencilState = &depth_stencil_state,
		.pColorBlendState = &color_blend_state,
		.pDynamicState = &dynamic_state,
		.layout = layout,
		.renderPass = render_pass,
	};

	const VkResult create = vkCreateGraphicsPipelines(device, cache, 1, &create_info, nullptr, &pipeline);
	if (create != VK_SUCCESS)
	{
		fprintf(stderr, "Failed to create a Vulkan graphics pipeline.\n");
	}

	return pipeline;
}

static VkImageMemoryBarrier
image_barrier(const VkImage image,
			  const VkAccessFlags source_mask, const VkImageLayout old_layout,
			  const VkAccessFlags destination_mask, const VkImageLayout new_layout)
{
	const VkImageMemoryBarrier barrier = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		.srcAccessMask = source_mask,
		.dstAccessMask = destination_mask,
		.oldLayout = old_layout,
		.newLayout = new_layout,
		.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.image = image,
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.levelCount = VK_REMAINING_MIP_LEVELS,
			.layerCount = VK_REMAINING_ARRAY_LAYERS,
		},
	};

	return barrier;
}

static void
cleanup_swapchain(const VkDevice device, const VkSwapchainKHR swapchain, const uint32_t images_count,
				  const VkImageView* image_views)
{
	vkDeviceWaitIdle(device);

	for (uint32_t i = 0; i < images_count; ++i)
	{
		vkDestroyImageView(device, image_views[i], nullptr);
	}

	vkDestroySwapchainKHR(device, swapchain, nullptr);
}

static void
create_swapchain(const VkDevice device,
				 const VkSurfaceKHR surface, const VkSurfaceCapabilitiesKHR surface_capabilities,
				 VkSwapchainKHR& swapchain,
				 VkImage (&images)[16], VkImageView (&image_views)[16],
				 uint32_t& images_count)
{
	assert(device && surface);

	VkSwapchainKHR old_swapchain = swapchain;
	swapchain = create_swapchain(device, surface, surface_capabilities, old_swapchain);
	assert(swapchain);

	cleanup_swapchain(device, old_swapchain, images_count, image_views);

	vkGetSwapchainImagesKHR(device, swapchain, &images_count, images);

	for (uint32_t i = 0; i < images_count; ++i)
	{
		image_views[i] = create_image_view(device, images[i]);
		assert(image_views[i]);
	}
}

static void
create_graphics_pipeline(VkRenderPass& render_pass, VkPipeline& pipeline,
						 VkFramebuffer (&framebuffers)[16],
						 const VkDevice device, const VkPipelineCache cache,
						 const VkShaderModule vertex, const VkShaderModule fragment,
						 const VkImageView (&image_views)[16], const uint32_t images_count,
						 const uint32_t width, const uint32_t height)
{
	assert(device &&
		   // cache &&
		   vertex && fragment);

	vkDeviceWaitIdle(device);

	vkDestroyPipeline(device, pipeline, nullptr);
	vkDestroyRenderPass(device, render_pass, nullptr);

	render_pass = create_render_pass(device);
	assert(render_pass);

	VkPipelineLayout layout = create_pipeline_layout(device);
	assert(layout);

	pipeline = create_pipeline(device, cache, layout, render_pass, vertex, fragment);
	assert(pipeline);
	vkDestroyPipelineLayout(device, layout, nullptr);

	for (uint32_t i = 0; i < images_count; ++i)
	{
		vkDestroyFramebuffer(device, framebuffers[i], nullptr);
		framebuffers[i] = create_framebuffer(device, render_pass, image_views[i], width, height);
		assert(framebuffers[i]);
	}
}

int main()
{
	glfwInit();

	VkInstance instance = create_instance();
	assert(instance);

	VkDevice device = create_device(instance);
	assert(device);

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow* window = glfwCreateWindow(1024, 768, "deferred", nullptr, nullptr);
	assert(window);

	VkSurfaceKHR surface = create_surface(instance, window);
	assert(surface);

	VkSurfaceCapabilitiesKHR surface_capabilities = {};
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(PhysicalDevice, surface, &surface_capabilities);

	VkSwapchainKHR swapchain = VK_NULL_HANDLE;
	VkImage swapchain_images[16] = {};
	VkImageView swapchain_image_views[16] = {};
	VkFramebuffer swapchain_framebuffers[16] = {};
	uint32_t swapchain_images_count = countof(swapchain_images);
	create_swapchain(device, surface, surface_capabilities, swapchain,
					 swapchain_images, swapchain_image_views,
					 swapchain_images_count);
	assert(swapchain);

	VkShaderModule triangle_vs = create_shader(device, SHADER_DIR "triangle.vert.spv");
	assert(triangle_vs);

	VkShaderModule triangle_fs = create_shader(device, SHADER_DIR "triangle.frag.spv");
	assert(triangle_fs);

	VkPipelineCache pipeline_cache = VK_NULL_HANDLE;

	VkRenderPass render_pass = VK_NULL_HANDLE;
	VkPipeline triangle_pipeline = VK_NULL_HANDLE;
	create_graphics_pipeline(render_pass, triangle_pipeline, swapchain_framebuffers, device, pipeline_cache,
							 triangle_vs, triangle_fs,
							 swapchain_image_views, swapchain_images_count,
							 surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height);
	assert(render_pass && triangle_pipeline);

	VkSemaphore acquisition_semaphore = create_semaphore(device);
	assert(acquisition_semaphore);

	VkSemaphore release_semaphore = create_semaphore(device);
	assert(release_semaphore);

	VkQueue queue = VK_NULL_HANDLE;
	vkGetDeviceQueue(device, QueueFamilyIndex, 0, &queue);

	VkCommandPool command_pool = create_command_pool(device);
	assert(command_pool);

	VkCommandBuffer command_buffer = create_command_buffer(device, command_pool);
	assert(command_buffer);

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		uint32_t image_index = 0;
		const VkResult acquire = vkAcquireNextImageKHR(device, swapchain, ~0ull, acquisition_semaphore, VK_NULL_HANDLE, &image_index);
		if (acquire == VK_ERROR_OUT_OF_DATE_KHR)
		{
			vkGetPhysicalDeviceSurfaceCapabilitiesKHR(PhysicalDevice, surface, &surface_capabilities);
			create_swapchain(device, surface, surface_capabilities, swapchain,
							 swapchain_images, swapchain_image_views, swapchain_images_count);
			create_graphics_pipeline(render_pass, triangle_pipeline, swapchain_framebuffers, device, pipeline_cache,
									 triangle_vs, triangle_fs,
									 swapchain_image_views, swapchain_images_count,
									 surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height);
		}

		vkResetCommandPool(device, command_pool, 0);

		constexpr VkCommandBufferBeginInfo buffer_begin_info = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
		};
		vkBeginCommandBuffer(command_buffer, &buffer_begin_info);

		VkImageMemoryBarrier render_begin_barrier = image_barrier(swapchain_images[image_index],
																  0, VK_IMAGE_LAYOUT_UNDEFINED,
																  VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
							 VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_DEPENDENCY_BY_REGION_BIT,
							 0, VK_NULL_HANDLE, 0, VK_NULL_HANDLE, 1, &render_begin_barrier);

		VkClearValue clear_values[1] = {
			{ 30.0f / 255.0f, 30.0f / 255.0f, 30.0f / 255.0f, 1.0f},
		};

		const VkRenderPassBeginInfo pass_begin_info = {
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			.renderPass = render_pass,
			.framebuffer = swapchain_framebuffers[image_index],
			.renderArea = {
				.extent = {
					.width = (uint32_t)surface_capabilities.currentExtent.width,
					.height = (uint32_t)surface_capabilities.currentExtent.height,
				},
			},
			.clearValueCount = countof(clear_values),
			.pClearValues = clear_values,
		};

		vkCmdBeginRenderPass(command_buffer, &pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);

		const VkViewport viewport = {
			.y = static_cast<float>(surface_capabilities.currentExtent.height),
			.width = static_cast<float>(surface_capabilities.currentExtent.width),
			.height = -static_cast<float>(surface_capabilities.currentExtent.height),
			.maxDepth = 1.0f,
		};
		vkCmdSetViewport(command_buffer, 0, 1, &viewport);

		const VkRect2D scissor = {
			.offset = { 0, 0 },
			.extent = { surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height },
		};
		vkCmdSetScissor(command_buffer, 0, 1, &scissor);

		vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, triangle_pipeline);

		vkCmdDraw(command_buffer, 3, 1, 0, 0);

		vkCmdEndRenderPass(command_buffer);

		VkImageMemoryBarrier render_end_barrier = image_barrier(swapchain_images[image_index],
																VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
																0, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
							 VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_DEPENDENCY_BY_REGION_BIT,
							 0, VK_NULL_HANDLE, 0, VK_NULL_HANDLE, 1, &render_end_barrier);

		vkEndCommandBuffer(command_buffer);

		constexpr VkPipelineStageFlags flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		const VkSubmitInfo submit_info = {
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &acquisition_semaphore,
			.pWaitDstStageMask = &flags,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffer,
			.signalSemaphoreCount = 1,
			.pSignalSemaphores = &release_semaphore,
		};
		vkQueueSubmit(queue, 1, &submit_info, VK_NULL_HANDLE);

		const VkPresentInfoKHR presentation_info = {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &release_semaphore,
			.swapchainCount = 1,
			.pSwapchains = &swapchain,
			.pImageIndices = &image_index,
		};
		const VkResult present = vkQueuePresentKHR(queue, &presentation_info);
		if (present == VK_ERROR_OUT_OF_DATE_KHR ||
			present == VK_SUBOPTIMAL_KHR)
		{
			vkGetPhysicalDeviceSurfaceCapabilitiesKHR(PhysicalDevice, surface, &surface_capabilities);
			create_swapchain(device, surface, surface_capabilities, swapchain,
							 swapchain_images, swapchain_image_views, swapchain_images_count);
			create_graphics_pipeline(render_pass, triangle_pipeline, swapchain_framebuffers, device, pipeline_cache,
									 triangle_vs, triangle_fs,
									 swapchain_image_views, swapchain_images_count,
									 surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height);
		}

		vkDeviceWaitIdle(device);
	}

	vkDestroySemaphore(device, acquisition_semaphore, nullptr);
	vkDestroySemaphore(device, release_semaphore, nullptr);
	vkDestroyCommandPool(device, command_pool, nullptr);

	vkDestroyShaderModule(device, triangle_vs, nullptr);
	vkDestroyShaderModule(device, triangle_fs, nullptr);

	for (uint32_t i = 0; i < swapchain_images_count; ++i)
	{
		vkDestroyFramebuffer(device, swapchain_framebuffers[i], nullptr);
	}
	cleanup_swapchain(device, swapchain, swapchain_images_count,
					  swapchain_image_views);

	vkDestroyPipeline(device, triangle_pipeline, nullptr);
	vkDestroyRenderPass(device, render_pass, nullptr);

	vkDestroySurfaceKHR(instance, surface, nullptr);
	vkDestroyDevice(device, nullptr);
	vkDestroyInstance(instance, nullptr);

	glfwDestroyWindow(window);

	return 0;
}
